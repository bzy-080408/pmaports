# Maintainer: Luca Weiss <luca@z3ntu.xyz>

_flavor="postmarketos-qcom-sc7280"
pkgname=linux-$_flavor
pkgver=6.7.0
pkgrel=3
pkgdesc="Mainline Kernel fork for SC7280/SM7325/QCM6490 devices"
arch="aarch64"
_carch="arm64"
url="https://github.com/z3ntu/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community
	"
makedepends="bash bison findutils flex installkernel openssl-dev perl"

_repo="linux"
_config="config-$_flavor.$arch"
_tag="v$pkgver-sc7280"

# Source
source="
	https://github.com/z3ntu/$_repo/archive/refs/tags/$_tag/$_repo-$_tag.tar.gz
	$_config
"
builddir="$srcdir/$_repo-$pkgver-sc7280"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/boot/dtbs
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
91976b128540662a672db3789b7a6bcbd24d6464594df8039366d4c1871995f94ccb3677ee19c97fbcff6663ec7f5034d94971e705780d01ee82f5a6008db0df  linux-v6.7.0-sc7280.tar.gz
62261b74e0529c0495cdc1747da9bcd15012de4969a07835744926686f172f7272d91f93645c735d918ba654cde7b1e4abb11048e4f45fdd312a3186772ae932  config-postmarketos-qcom-sc7280.aarch64
"
